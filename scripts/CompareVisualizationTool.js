/*
	@class: CompareVisualizationTool
	A class that handles the visualization of selected groups in COMPARE mode.

	@Protocols

	@author: Massimo De Marchi
	@copyright 2014
*/
function CompareVisualizationTool(delegate) {
	// PRIVATE ATTRIBUTES
	var self = this;
	var _delegate = delegate;

	// PUBLIC METHODS
	this.draw = function(selectedGroups, dataType, visualizationType) {
		var container = d3.select("#visualization-compare-wrapper");

		var communitiesGroups = container.selectAll(".communities_group").data(selectedGroups);

		// Update header
		communitiesGroups
			.select("header")
				.select("h1")
					.attr("class", function(group, index) {
						return "group-color" + (index +1);
					})
					.text(function(group) {
						return dataAggregator.getNameForSelection(group);
					});

		// Update chart
		communitiesGroups
			.select(".chart")
				.select("svg")
				.each(function(communities){
					var svg = d3.select(this);
					svg.html("");
					drawChart(svg, communities, dataType, visualizationType,selectedGroups);
				});

		// Add toolbar
		communitiesGroups
			.select("chart-options")
				.select("button")
					.on('click', function(d, i) {
						deleteSelectionAtIndex(i);
					});

		var newCommunities = communitiesGroups.enter()
								.append("div")
									.classed("communities_group", true);

		
		
		// Header
		newCommunities
			.append("header")
				.attr("class", function(group, index) {
						return "group-color" + (index +1);
					})
				.append("h1")
					.text(function(group) {
						return dataAggregator.getNameForSelection(group);
					});
		
		// Draw chart
		newCommunities
			.append("div")
				.classed("chart", true)
				.append("svg")
				.each(function(communities){
					var svg = d3.select(this);
					drawChart(svg, communities, dataType, visualizationType,selectedGroups);
				});

		// Add toolbar
		newCommunities
			.append("div")
				.classed("chart-options", true)
				.append("button")
					.text("Remove selection")
					.attr("id", "chart-delete-button")
					.on('click', function(d, i) {
						deleteSelectionAtIndex(i);
					});

		var removed = communitiesGroups.exit().remove();
	}
	











	// PRIVATE METHODS
	var drawChart = function(svg, communities, dataType, visualizationType,selectedGroups) {
		var chart;
		var data;
		var maxValue;

		// BARCHART
		if (visualizationType == VisualizationType.BARCHART || _delegate.getComparisonType() == ComparisonType.SHOWDIFFERENCE) {
			chart = new ColumnChart(svg);
			  
			// AGE
			if (dataType == DataType.AGE) {
				data = loadAgeData(communities);
				chart.setYAxisLabel("POPULATION");
				chart.setXAxisLabel("AGE");
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.setTitle("AGE DISTRIBUTION");
			}
			// GENDER 
			else if (dataType == DataType.GENDER) {
				data = loadGenderData(communities);
				chart.setYAxisLabel("POPULATION");
				chart.setXAxisLabel("GENDER");
				chart.setColors(["#67a9cf","#e9a3c9"]);
				chart.setTitle("GENDER DISTRIBUTION");
			}
			// PLACE OF ORIGIN 
			else if (dataType == DataType.PLACE_OF_ORIGIN) {
				chart = new BarChart(svg);
				data = loadPlaceOfOriginData(communities);
				chart.setColors(["#91bfdb"]);
				chart.setTitle("PLACE OF ORIGIN DISTRIBUTION");
				chart.removeZeroValues(true);
				chart.setMainComponentsNumber(4);
			}
			// RACE 
			else if (dataType == DataType.RACE) {
				chart = new BarChart(svg);
				data = loadRacesData(communities);
				chart.setColors(["#91bfdb"]);
				chart.setTitle("RACES DISTRIBUTION");
				chart.removeZeroValues(true);
				chart.setMainComponentsNumber(4);
			}
		}

		// PIECHART 
		else if (visualizationType == VisualizationType.PIECHART) {
			chart = new PieChart(svg);

			// AGE
			if (dataType == DataType.AGE) {
				data = loadAgeData(communities);
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.setTitle("AGE DISTRIBUTION");
			}
			// GENDER 
			else if (dataType == DataType.GENDER) {
				data = loadGenderData(communities);
				chart.setColors(["#67a9cf","#e9a3c9"]);
				chart.setTitle("GENDER DISTRIBUTION");
			}
			// PLACE OF ORIGIN 
			else if (dataType == DataType.PLACE_OF_ORIGIN) {
				data = loadPlaceOfOriginData(communities);
				chart.setTitle("PLACE OF ORIGIN DISTRIBUTION");
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.removeZeroValues(true);
				chart.setMainComponentsNumber(4);
			}
			// RACE 
			else if (dataType == DataType.RACE) {
				data = loadRacesData(communities);
				chart.setTitle("RACES DISTRIBUTION");
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.removeZeroValues(true);
				chart.setMainComponentsNumber(4);
			}
		}

		// TABLE CHART
		else if (visualizationType == VisualizationType.RAWTABLE) {
			chart = new TableChart(svg);

			// AGE
			if (dataType == DataType.AGE) {
				data = loadAgeData(communities);
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.setTitle("AGE DISTRIBUTION");
			}
			// GENDER 
			else if (dataType == DataType.GENDER) {
				data = loadGenderData(communities);
				chart.setColors(["#67a9cf","#e9a3c9"]);
				chart.setTitle("GENDER DISTRIBUTION");
			}
			// PLACE OF ORIGIN 
			else if (dataType == DataType.PLACE_OF_ORIGIN) {
				data = loadPlaceOfOriginData(communities);
				chart.setTitle("PLACE OF ORIGIN DISTRIBUTION");
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.removeZeroValues(true);
				chart.setMainComponentsNumber(4);
			}
			// PLACE OF ORIGIN 
			else if (dataType == DataType.RACE) {
				data = loadRacesData(communities);
				chart.setTitle("RACES DISTRIBUTION");
				chart.setColors(["#fbb4ae","#b3cde3","#ccebc5","#decbe4","#fed9a6"]);
				chart.removeZeroValues(true);
				chart.setMainComponentsNumber(4);
			}
		}
		
		// Set scale
		if (_delegate.getComparisonType() == ComparisonType.SHOWDIFFERENCE) {
			var tmp;
			var max = 0;
			// AGE
			if (dataType == DataType.AGE) {
				selectedGroups.forEach(function(group) {
					tmp = d3.max(loadAgeData(group), function(d){
						return parseInt(d.value);
					});
					if (tmp > max) {
						max = tmp;
					}
				});
			}
			// GENDER 
			else if (dataType == DataType.GENDER) {
				selectedGroups.forEach(function(group) {
					tmp = d3.max(loadGenderData(group), function(d){
						return parseInt(d.value);
					});
					if (tmp > max) {
						max = tmp;
					}
				});
			}
			// PLACE OF ORIGIN 
			else if (dataType == DataType.PLACE_OF_ORIGIN) {
				selectedGroups.forEach(function(group) {
					tmp = d3.max(loadPlaceOfOriginData(group), function(d){
						return parseInt(d.value);
					});
					if (tmp > max) {
						max = tmp;
					}
				});
			}
			// RACE 
			else if (dataType == DataType.RACE) {
				selectedGroups.forEach(function(group) {
					tmp = d3.max(loadRacesData(group), function(d){
						return parseInt(d.value);
					});
					if (tmp > max) {
						max = tmp;
					}
				});
			}

			chart.setMaxValue(max);
		}
		chart.setData(data);
		chart.draw();
	}	

	var loadAgeData = function(communities) {
		var data = [];

		communities.forEach(function(community) {
			var distribution = dataManager.getAgeDistributionForCommunity(community, 4);
			distribution.forEach(function(ageInterval, i) {
				if (data[i] == undefined) {
					data[i] = {
						label : ageInterval.range.toString(),
						value : parseInt(ageInterval.number)
					}	
				} else {
					data[i].value += parseInt(ageInterval.number);
				}
			});
		});

		return data;
	}

	var loadGenderData = function(communities) {
		var data = [];

		var totalMale = 0;
		var totalFemale = 0;
		communities.forEach(function(community) {
			var distribution = dataManager.getGenderDistributionForCommunity(community);
			
			totalMale += parseInt(distribution.male);
			totalFemale += parseInt(distribution.female);
		});

		data.push({
			label : "male",
			value : totalMale
		});

		data.push({
			label : "female",
			value : totalFemale
		});

		return data;
	}

	var loadRacesData = function(communities) {
		var data = [];

		communities.forEach(function(community) {
			var raceDistribution = dataManager.getRacesDistributionForCommunity(community);
			var races = d3.keys(raceDistribution);
			races.forEach(function(race, i) {
				if (data[i] == undefined) {
					data[i] = {
						label : race,
						value : parseInt(raceDistribution[race])
					};	
				} else {
					data[i].value += parseInt(raceDistribution[race]);
				}
			});
		});

		dio = data;

		return data;
	}

	var loadPlaceOfOriginData = function(communities) {
		var data = [];

		communities.forEach(function(community) {
			var commDistribution = dataManager.getCountriesDistributionForCommunity(community);
			commDistribution.forEach(function(personPerCountry, i) {
				if (data[i] == undefined) {
					data[i] = {
						label : personPerCountry.country,
						value : parseInt(personPerCountry.number)
					};	
				} else {
					data[i].value += parseInt(personPerCountry.number);
				}
			});
		});

		return data;
	}

	var deleteSelectionAtIndex = function(index) {
		_delegate.removeSelectionAtIndex(index);
	}

	var init = function() {
	} ();
}











