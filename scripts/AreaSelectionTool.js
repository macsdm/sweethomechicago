/*
	@class: AreaSelectionTool
	A class that manages interactive UI for area selection handling.

	@Protocols
	- userSelectHighlighted
	- getHighlightedIds

	@author: Massimo De Marchi
	@copyright 2014
*/
function AreaSelectionTool(containerBox, selectorDelegate) {
	// PRIVATE ATTRIBUTES
	var self = this;
	var _container = containerBox;

	var _selectorDelegate = selectorDelegate;

	// PUBLIC METHODS
	this.setSelectorDelegate = function(delegate) {
		_selectorDelegate = delegate;
	}

	this.updateUI = function() {
		drawToolbar();
		drawDistrictButtons();
	}

	// PRIVATE METHODS
	/*
	var draw = function() {
		drawToolbar();
		drawDistrictButtons();
	}*/

	var drawToolbar = function() {
		var selectButton = _container.select("#select-current-highlight");

		if (_selectorDelegate.getHighlightedIds().length == 0) {
			selectButton.text("All");
			selectButton.on('click', function() {
				_selectorDelegate.selectAll();
			});
		} else {
			selectButton.text("Select");
			selectButton.on('click', function() {
				_selectorDelegate.userSelectHighlighted();
			});
		}

		var visualizeButton = _container.select("#visualize");
		visualizeButton.on('click', function() {
			console.log("groups: " + _selectorDelegate.getSelectionInProgress().length);
			if (_selectorDelegate.getSelectionInProgress().length > 0) {
				_selectorDelegate.visualizeCurrentSelection();
			}
		});
	}

	var drawDistrictButtons = function() {
		var districts = dataManager.getChicagoDistricts();
		var districtsData = [];

		for (var i = 0; i < districts.length; i++) {
			var communitiesIds = dataManager.getCommunitiesIdForDistrict(districts[i]);

			if (!_selectorDelegate.getHighlightedIds().intersects(communitiesIds)) {
				communitiesIds = [];
			}
			districtsData.push({
				name : districts[i],
				communities : communitiesIds
			}); 
		}

		var districtContainers = 
		d3.select("#area-button-wrapper")
			.selectAll(".district-button-container")
			.data(districtsData)
			.attr("class", function(d) {
				return _selectorDelegate.stylesForDistrict(d.name).join(" ");
			});
		
		districtContainers.enter()
				.append("div").classed("district-button-container", true)
					.append("button").text(function(d) {
						return d.name;
					})
					.classed("district-button", true)
					.on('click', function(d) {
						var communities = dataManager.getCommunitiesIdForDistrict(d.name);
						districtHighlighted(communities);
					});

		var communitiesContainers =
		districtContainers.selectAll(".community-button-container")
			.data(function(d) {
				return d.communities;
			})
			.attr("class", function(d) {
				return _selectorDelegate.stylesForCommunity(d).join(" ");
			});

		communitiesContainers.enter()
				.append("div")
					.attr("class", function(d) {
						return _selectorDelegate.stylesForCommunity(d).join(" ");
					})
					.append("button")
					.text(function(d) {
						return dataManager.getCommunityNameForId(d);
					})
					.classed("community-button", true)
					.on('click', function(d) {
						communityHighlighted(d);
					});

		communitiesContainers.exit()
				.remove();
	}

	var districtHighlighted = function(ids) {
		// Remove previous selection
		if (_selectorDelegate.getHighlightedIds().equals(ids)) {
			_selectorDelegate.userDidHighlight(null);
		} else {
			_selectorDelegate.userDidHighlight(ids);
		}
	}

	var communityHighlighted = function(id) {
		if (_selectorDelegate.getHighlightedIds().equals([id])) {
			var district = dataManager.districtOfCommunity(id);
			var communities = dataManager.getCommunitiesIdForDistrict(district);
			_selectorDelegate.userDidHighlight(communities);
		} else {
			_selectorDelegate.userDidHighlight(id);
		}
	}

	var init = function() {
		self.updateUI();
	} ();
}








