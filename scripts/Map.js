/*
	@class: Map
	A class useful for handling chicago geo_json map.
	The map is builded under the assumption that givin scale 1 
	the geometry fits the svg canvas.

	See "map.css" for styling

	@Protocols
	Delegate must implement 
	- userDidHighlight(id) -> gets called when user click on some area of the map, the area id is passed
	- classForId(id) -> Delegate class should implement this protocol to return an array of style 
						classes for the given community area
	
	
	@author: Massimo De Marchi
	@copyright 2014
*/

// svg passed as a d3 selection
function Map(svgBox, geoJSONdata) {
	// PRIVATE ATTRIBUTES
    var svg = svgBox;
    var svgW = svg[0][0].clientWidth, 	// Width and Height of the svg
    	svgH = svg[0][0].clientHeight;	
    //var mapPath = geoJSONPath;
    var geoJSON = geoJSONdata;

    // Formatted that store indexes and geometry
    var communities_dict = {};

    // Map position and scaling
    var scale = 1;
    var center = [0,0];
    var offset = [svgW/2, svgH/2];
    var mapWidth = 0;
    var mapHeight = 0;

    var that = this;
    var self = this; // Not the best class I ever made
    var delegate;

    // PUBLIC METHODS
    this.setScale = function(newScale) {
    	scale = newScale;
    	draw();
    };

    this.getScale = function() {
    	return scale;
    };

    this.setDelegate = function(newDelegate) {
    	delegate = newDelegate;
    };


    










    this.zoomTo = function(ids) {
    	if (ids.length > 0) {
    		var groupBound = boundsForIds(ids);
	    	center = [meanLongitude(groupBound), meanLatitude(groupBound)];
	    	scale = Math.min(mapWidth / longitudeDistance(groupBound), mapHeight / latitudeDistance(groupBound));
	    	draw(true);
    	} else {
    		self.zoomOut();
    	}
    	
    	/*
    	var b = d3.geo.bounds(communities_dict[id]);
    	center = [meanLongitude(b), meanLatitude(b)];
    	scale = Math.min(mapWidth / longitudeDistance(b), mapHeight / latitudeDistance(b));
    	draw();*/
    }

    this.zoomOut = function() {
    	var allCommunities = dataManager.getAllCommunitiesIds();
    	self.zoomTo(allCommunities);
    }








    this.updateUI = function() {
    	svgW = svg[0][0].clientWidth; 	
    	svgH = svg[0][0].clientHeight;
		offset = [svgW/2, svgH/2];

		var highlighted = delegate.getHighlightedIds();

		if (highlighted.length == 1) {
			var district = dataManager.districtOfCommunity(highlighted[0]);
			var communities = dataManager.getCommunitiesIdForDistrict(district);
			highlighted = communities;
		}

		self.zoomTo(highlighted);

		draw();
    }













    // PRIVATE METHODS





    // Handles click on an area
    var clickOn = function(sender) {
    	//that.zoomTo(sender.id);
    	
    	var highlighted = delegate.getHighlightedIds();

    	// District is highlighted
    	if (highlighted.length > 1) {
    		var indexOfClicked = highlighted.indexOf(sender.id);
    		if (indexOfClicked == -1) {
    			// Click out of the district
    			self.zoomOut();
    			delegate.userDidHighlight(null);
    		} else {
    			delegate.userDidHighlight(sender.id);
    		}
    	} else if(highlighted.length == 1) {
    		var district = dataManager.districtOfCommunity(highlighted[0]);
    		var communities = dataManager.getCommunitiesIdForDistrict(district);

    		var indexOfClicked = communities.indexOf(sender.id);
    		if (indexOfClicked == -1) {
    			// Click out of the district
    			self.zoomOut();
    			delegate.userDidHighlight(null);
    		} else {
    			if (highlighted[0] == sender.id) {
    				delegate.userDidHighlight(communities);	
    			} else {
    				delegate.userDidHighlight(sender.id);	
    			}    			
    		}
    	} else {
    		var district = dataManager.districtOfCommunity(sender.id);
    		var communities = dataManager.getCommunitiesIdForDistrict(district);
    		self.zoomTo(communities);
    		delegate.userDidHighlight(communities);	
    	}
    	/*
    	if (delegate.getHighlightedIds().equals([sender.id])) {
    		delegate.userDidHighlight(null);
    	} else {
    		delegate.userDidHighlight(sender.id);	
    	}*/
    	
    }

    var boundsForIds = function(ids) {
    	var groupBoundaries = [];
    	ids.forEach(function(id) {
    		var b = d3.geo.bounds(communities_dict[id]);
    		groupBoundaries.push(b);
    	});
    	
    	var raw = _.flatten(groupBoundaries);

    	var longitudes = raw.filter(function(el,i) {
    		return (i % 2 == 0);
    	});
    	var latitudes = raw.filter(function(el,i) {
    		return (i % 2 != 0);
    	});

    	var minLon = d3.min(longitudes);
    	var minLat = d3.min(latitudes);
    	var maxLon = d3.max(longitudes);
    	var maxLat = d3.max(latitudes);

    	var groupBound = [[minLon, minLat], [maxLon, maxLat]];

    	return groupBound;
    }

    var geoDistance = function(points) {
		var R = 6371; // km
		var lat1 = points[0][1];
		var lat2 = points[1][1];
		var lon1 = points[0][0];
		var lon2 = points[1][0];
		var phi1 = lat1.toRadians();
		var phi2 = lat2.toRadians();
		var delta_phi = (lat2 - lat1).toRadians();
		var delta_lambda = (lon2 - lon1).toRadians();

		var a = Math.sin(delta_phi/2) * Math.sin(delta_phi/2) +
		Math.cos(phi1) * Math.cos(phi2) *
		Math.sin(delta_lambda/2) * Math.sin(delta_lambda/2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		var d = R * c;
		return d;
	}

	var deltaLongitude = function(bounds) {
		return Math.abs(bounds[1][0] - bounds[0][0]);//[[bounds[0][0],bounds[0][1]],[bounds[1][0],bounds[0][1]]];
	}

	var deltaLatitude = function(bounds) {
		return Math.abs(bounds[1][1] - bounds[0][1]);//[[bounds[0][0],bounds[0][1]],[bounds[0][0],bounds[1][1]]];
	}

	// Km as result unit measure
	var longitudeDistance = function(bounds) {
		var lonPoints = [[bounds[0][0],bounds[0][1]],[bounds[1][0],bounds[0][1]]];
		return geoDistance(lonPoints);
	}

	// Km as result unit measure
	var latitudeDistance = function(bounds) {
		var latPoints = [[bounds[0][0],bounds[0][1]],[bounds[0][0],bounds[1][1]]];
		return geoDistance(latPoints);
	}

	var meanLongitude = function(bounds) {
		return (bounds[1][0]+bounds[0][0])/2;
	}

	var meanLatitude = function(bounds) {
		return (bounds[1][1]+bounds[0][1])/2;
	}

	var longitudeScreenRatio = function() {
		var bounds = d3.geo.bounds(geoJSON);
		return (deltaLongitude(bounds) / svgW);
	}

	var latitudeScreenRatio = function() {
		var bounds = d3.geo.bounds(geoJSON);
		return (deltaLatitude(bounds) / svgH);
	}

	var resize = function() {
    	console.log("resize");
    	svgW = svg[0][0].clientWidth;
		svgH = svg[0][0].clientHeight;
		offset = [svgW/2, svgH/2];
		draw();
    };











    // Called for drawing the map
    var draw = function(animate) {
    	var adjFactor = 4700; // is a correction scaling factor yet to be understood;
		var s = Math.min(svgW / mapWidth, svgH / mapHeight);
		s *= adjFactor;	// Adjustment factor

		var projection = d3.geo.mercator().scale(s * scale).center(center).translate(offset);
		
		var path = d3.geo.path().projection(projection);

		// community_dict[id] = feature

		var paths = svg.selectAll("path").data(geoJSON.features);

		paths
		.attr("class", function(d) {
				var classes = delegate.classForId(d.id).join(" ");
				var district = dataManager.districtOfCommunity(d.id);
				district = district.replace(/ /g, "");
				classes += " " + district;
				return classes;
			});

		paths.enter()
			.append("path")
			.attr("class", function(d) {
				var classes = delegate.classForId(d.id).join(" ");
				var district = dataManager.districtOfCommunity(d.id);
				district = district.replace(/ /g, "");

				classes += " " + district;
				return classes;
			})
			.on('click', function(d,i) {
				d3.event.stopPropagation();
				clickOn(d);
			});
		
		paths
		.attr("stroke", "#95a5a6")//#34495E")
		.attr("stroke-width", "1.0");

		//var labels = svg.selectAll(".labels").data(geoJSON.features);


		

		/*
		[{label: l, features: f}]
		*/

		var highlighted = delegate.getHighlightedIds();
		var labelData = [];

		if (highlighted.length > 0) {
			highlighted.forEach(function(id) {
				labelData.push({
					label: dataManager.getCommunityNameForId(id),
					features: communitiesFeatures[id]
				});
			});
		} else {
			var districts = dataManager.getChicagoDistricts();

			districts.forEach(function(district) {
				var communities = dataManager.getCommunitiesIdForDistrict(district);
				var dFeatures = [];
				communities.forEach(function(id) {
					dFeatures.push(communitiesFeatures[id]);
				});

				labelData.push({
					label: district,
					features: {
						type: "FeatureCollection",
						features : dFeatures
					}
				});
			});
		}

		var labels = svg
						.selectAll(".place-label")
						.data(labelData);

		labels
			.attr("transform", function(d) { 
	    				return "translate(" + path.centroid(d.features) + ")"; 
	    			})
	    			.attr("dy", ".35em")
	    			.attr("text-anchor", "middle")
	    			.text(function(d) { return d.label; });

		labels
	  		.enter()
	  			.append("text")
	    			.attr("class", "place-label")
	    			.attr("transform", function(d) { 
	    				return "translate(" + path.centroid(d.features) + ")"; 
	    			})
	    			.attr("dy", ".35em")
	    			.style("font-size", "8px")
	    			.text(function(d) { return d.label; });

	    labels.exit().remove();


		if (arguments.length == 1 && animate) {
			// Animate map to view
			console.log("animate");
			paths.transition().duration(2000)
				.attr("d", path)
		} else {
			paths
				.attr("d", path)
		}
    }

	/* Init function */
    var init = function() {
    	communitiesFeatures = {};
		// Restructure data for faster search
    	for(var f in geoJSON.features) {
    		communities_dict[geoJSON.features[f].id] = geoJSON.features[f].geometry;
    		communitiesFeatures[geoJSON.features[f].id] = geoJSON.features[f];
   		}

    	// Center to whole map
   		var b = d3.geo.bounds(geoJSON);
    	mapWidth = longitudeDistance(b);
    	mapHeight = latitudeDistance(b);
    	center = [meanLongitude(b), meanLatitude(b)];

    	svg.attr("class", "geo-map");
    	d3.select(window).on('resize', resize);

    	svg.on('click', function() {
    		self.zoomOut();
    		delegate.userDidHighlight(null);
    	});

    	/* Handling gestures */
    	/*
	    var dragListener = d3.behavior.drag()
	    	.on("drag", function() {
	    		//offset = [offset[0] + d3.event.dx, offset[1] + d3.event.dy];
	    		center = [center[0] -longitudeScreenRatio() * d3.event.dx, center[1] + latitudeScreenRatio() * d3.event.dy];
	    		draw();
	    	});*/

	    // create the zoom listener
	    /*
		var zoomListener = d3.behavior.zoom()
			.scale(scale)
			.size([svgW, svgH])
			.scaleExtent([1, 4])
			.on("zoom", function() {
				//console.log(d3.event.scale);
				scale = d3.event.scale;
				draw();
			});
    	svg.call(dragListener);
    	svg.call(zoomListener);
    	*/
    } ();
}









