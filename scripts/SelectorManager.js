/*
	@class: SelectorManager
	Class that keeps track of area selection.

	@Protocols

	@author: Massimo De Marchi
	@copyright 2014
*/
function SelectorManager() {
	// PRIVATE ATTRIBUTES
	_currentHighlightedAreas = [];
	_currentSelectedAreas = [];
	_currentVisualizedAreas = [];

	// PUBLIC METHODS

	//@PROTOCOLS
	this.userDidHighlight = function(ids) {
		if (ids == null) {
			_currentHighlightedAreas = [];
		} else if (typeof(ids) == "number") {
			_currentHighlightedAreas = [ids];
		} else if (ids instanceof Array) {
			_currentHighlightedAreas = ids;
		}
		updateUI();
	}

	this.selectAll = function() {
		var allIds = dataManager.getAllCommunitiesIds();
		_currentSelectedAreas = allIds;

		updateUI();
	}

	// Groups currently being visualized
	this.getSelectedGroups = function() {
		return _currentVisualizedAreas;
	}

	this.getSelectionInProgress = function() {
		return _currentSelectedAreas;
	}

	this.getHighlightedIds = function() {
		return _currentHighlightedAreas;
	}

	this.isHighlighted = function(id) {
		return self.getHighlightedIds.indexOf(id) != -1;
	}

	this.classForId = function(id) {
		var classes = [];

		if (_currentSelectedAreas.intersects([id])) {
			classes.push("area_selected" + (_currentVisualizedAreas.length +1));
		}
		else if (_currentHighlightedAreas.intersects([id])) {
			classes.push("area_highlighted");
		} 
		else {
			_currentVisualizedAreas.forEach(function(el, index) {
				if (el.intersects([id])) {
					classes.push("area_selected" + (index +1));
				}
			});
		}
		
		return classes;
	}

	this.stylesForDistrict = function(districtName) {
		var classes = ["district-button-container"];

		var communitiesIds = dataManager.getCommunitiesIdForDistrict(districtName);

		if (_currentHighlightedAreas.equals(communitiesIds)) {
			classes.push("area_highlighted");
		}

		if (_.intersection(_currentSelectedAreas, communitiesIds).equals(communitiesIds)) {
			classes.push("area_selected" + (_currentVisualizedAreas.length +1));
		}

		_currentVisualizedAreas.forEach(function(el, index) {
			if (el.containsAllElementsOfArray(communitiesIds)/*_.intersection(el, communitiesIds).equals(communitiesIds)*/) {
				classes.push("area_selected" + (index +1));
			}
		});

		return classes;
	}

	this.stylesForCommunity = function(id) {
		var classes = ["community-button-container"];

		if (_currentHighlightedAreas.intersects([id])) {
			classes.push("area_highlighted");
		}

		if (_currentSelectedAreas.intersects([id])) {
			classes.push("area_selected" + (_currentVisualizedAreas.length +1));
		}

		_currentVisualizedAreas.forEach(function(el, index) {
			if (el.intersects([id])) {
				classes.push("area_selected" + (index +1));
			}
		});
		
		return classes;
	}

	// PIN
	this.userSelectHighlighted = function() {
		_currentSelectedAreas = _currentSelectedAreas.concat(_currentHighlightedAreas);
		//_currentHighlightedAreas = [];
		updateUI();
	}

	this.visualizeCurrentSelection = function() {
		// TODO
		var i = _currentVisualizedAreas.length;
		_currentVisualizedAreas[i] = _currentSelectedAreas;
		_currentSelectedAreas = [];

		notificationCenter.dispatch(Notifications.VISUALIZATION_GROUPS_CHANGED);
		updateUI();
	}

	this.removeVisualizationAtIndex = function(index) {
		_currentVisualizedAreas[index] = false;
		_currentVisualizedAreas = _.compact(_currentVisualizedAreas);

		notificationCenter.dispatch(Notifications.VISUALIZATION_GROUPS_CHANGED);
		updateUI();
	}

	// PRIVATE METHODS

	var updateUI = function() {
		chicagoMap.updateUI();
		areaSelectionTool.updateUI();
	}

	var init = function() {
		_currentHighlightedAreas = [];
		_currentSelectedAreas = [];
		_currentVisualizedAreas = [];
		_currentVisualizedAreas.push(dataManager.getAllCommunitiesIds());
	} ();
};









