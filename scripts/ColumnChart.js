/*
@class: ColumnChart
A parametric class that handle the displaying of ColumnCharts.

@Data format
{
	label: ..
	value: ..
}

@Protocols

@author: Massimo De Marchi
@copyright 2014
*/
function ColumnChart(svg) {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _svg = svg;
	var _boxWidth = 600;
	var _boxHeight = 300;

	var _data;
	var _title;
	var _xAxisLabel;
	var _yAxisLabel;
	var _colors;
	var _maxValue;

	// PUBLIC METHODS
	this.draw = function() {
		var margin = {top: 50, right: 100, bottom: 40, left: 150},
		width = _boxWidth - margin.left - margin.right,
		height = _boxHeight - margin.top - margin.bottom;

		var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);

		var y = d3.scale.linear()
		.range([height, 0]);

		var xAxis = d3.svg.axis()
						.scale(x)
						.orient("bottom")
						.outerTickSize(1);

		var yAxis = d3.svg.axis()
						.scale(y)
						.orient("left")
						.ticks(6)
						.tickFormat(formatNumber)
						.outerTickSize(1);

		var color = d3.scale.ordinal()
			.range(_colors);

		// Chart container
		var chart = _svg
						.append("g")
						.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		x.domain(_data.map(function(d) { return d.label; }));

		var domain;
		if (_maxValue != undefined) {
			domain = [0, parseInt(_maxValue)];
		} else {
			domain = [0, d3.max(_data, function(d) { return d.value; })];
		}
					
		y.domain(domain);

		// x axis
		chart.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + height + ")")
			.call(xAxis)
				.append("text")
			    	.attr("transform", "translate(" + (width +10) + ",0)")
				    .attr("y", 6)
				    .attr("dy", ".71em")
				    .style("text-anchor", "start")
				    .text(function() {
				    	return _xAxisLabel;
				    });

		// y axis
		chart
			.append("g")
				.attr("class", "y axis")
				.call(yAxis)
					.append("text")
				    	.attr("transform", "translate(0, "+ (-30) +" )")
					    .attr("y", 6)
					    .attr("dy", ".71em")
					    .style("text-anchor", "end")
					    .text(function() {
					    	return _yAxisLabel;
					    });

		// Bars
		chart
		.append("g")
			.selectAll(".bar")
				.data(_data)
				.enter()
					.append("rect")
						.attr("class", "bar")
						.attr("x", function(d) { 
							return x(d.label); 
						})
						.attr("y", function(d) { 
							return y(d.value); 
						})
						.attr("height", function(d) { 
							return height - y(d.value); 
						})
						.attr("width", x.rangeBand())
						.style("fill", function(d) { 
							return color(d.label); 
						});
		
		var gTitle = _svg.append("g")
						.attr("transform", "translate(" + _boxWidth/2 + ",0)");
		gTitle
			.append("text")
        		.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return _title; 
				});
	}

	this.setData = function(newData) {
		_data = newData;
	}

	this.setTitle = function(newTitle) {
		_title = newTitle;
	}

	this.setXAxisLabel = function(label) {
		_xAxisLabel = label;
	}

	this.setYAxisLabel = function(label) {
		_yAxisLabel = label;
	}

	this.setColors = function(newColors) {
		_colors = newColors;
	}

	this.setMaxValue = function(newMaxValue) {
		_maxValue = newMaxValue;
	}

	// PRIVATE METHODS
	var type = function(d) {
		d.value = +d.value; // coerce to number
		return d;
	}

	var init = function() {
		_svg
			.attr("viewBox", "0 0 " + _boxWidth + " " + _boxHeight)
			.attr("preserveAspectRatio","xMidYMid meet");
		_xAxisLabel = "";
		_yAxisLabel = "";
	} ();
}










