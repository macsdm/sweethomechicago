/*
	@class: OptionMenuTool
	A class that manages a menu where the user can select option.
	When use option change this class notify the delegate with the option selected.

	@Protocols
	- optionSelected(index) ==> delegate class must implement this method

	@author: Massimo De Marchi
	@copyright 2014
*/
function OptionsMenuTool (svg, delegate) {
	// PRIVATE ATTRIBUTES
	var self = this;
	var delegate = delegate;

	var _svg = svg;
	var _boxWidth = 300;
	var _boxHeight = 600;

	// Data
	var _colors = ["#000"];
	var _buttonLabels = [];
	var _indexSelected = 0;










	// PUBLIC METHODS
	this.draw = function() {
		var margin = {top: 10, right: 10, bottom: 50, left: 10};
		var width = _boxWidth - margin.left - margin.right;
		var height = _boxHeight - margin.top - margin.bottom;

		var buttonHeight = _boxHeight / 15;
		var buttonPaddingTop = buttonHeight / 10;
		var buttonNumber = _buttonLabels.length;
		var wrapperHeight = buttonNumber * (buttonHeight + buttonPaddingTop);

		_svg.html("");

		var gMenu = _svg.append("g")
							.classed("option-menu-wrapper", true)
							.attr("transform", function() {
								var x = margin.left;
								var y = margin.top + height - wrapperHeight;
								return "translate(" + x + "," + y + ")";
							});

		var gButtons = gMenu.selectAll(".gButtons").data(_buttonLabels);

		// New
		gButtons
			.enter()
				.append("g")
					.classed("gButtons", true)
					.attr("transform", function(d, i) {
						var x = 0;
						var y = wrapperHeight - buttonHeight - (i * (buttonHeight + buttonPaddingTop));
						return "translate(" + x + "," + y + ")";
					});

		var outerRadius = buttonHeight/2;
		var outLine = buttonHeight/10;
		var innerRadius = outerRadius -outLine;
		gButtons
			.append("circle")
				.attr("cx", outerRadius)
				.attr("cy", outerRadius)
				.attr("r", outerRadius)
                .style("fill", function(d, i) {
                	if (_colors.length < buttonNumber) {
                		return _colors[0];
                	}
                	return _colors[i]; 
                })
                .style("stroke", "#34495E")
                .style("stroke-width", function(d,i) {
                	return i == _indexSelected ? outLine : 0;
                })
                .on("click", function(d,i) {
                	if (i != _indexSelected) {
                		_indexSelected = i;
                		delegate.optionSelected(i);
                	}
                });

        var labelsPaddingLeft = outerRadius/2;
        gButtons
        	.append("text")
        		.attr("transform", function(d, i) { 
        			var x = outerRadius *2 + labelsPaddingLeft;
        			var y = buttonHeight/2;
					return "translate(" + x + "," + y + ")"; 
				})
				.attr("dy", ".35em")
				.style("text-anchor", "start")
				.text(function(d) { 
					return d; 
				});
	}

	this.setColors = function(colors) {
		_colors = colors;
	}

	this.setButtonLabels = function(buttonLabels) {
		_buttonLabels = buttonLabels;
	}

	this.getSelectedIndex = function() {
		return _indexSelected;
	}







	// PRIVATE METHODS
	var init = function() {
		_svg
			.attr("viewBox", "0 0 " + _boxWidth + " " + _boxHeight)
			.attr("preserveAspectRatio","xMidYMid meet");
	} ();
}

















