/*
	@class: DataManager
	A class useful for loading Chicago data.

	@Protocols
	Delegate must implement 
	- dataLoaded() --> gets called when the data manager has finished to load all data

	@author: Massimo De Marchi
	@copyright 2014
*/
function DataManager() {
	// PRIVATE ATTRIBUTES
	var self = this;
	var _delegate;

	var _operationsQueue;

	// Map attributes
	var _geoJSON;

	// Chicago districts and communities
	var _communitiesByDistrict;
	var _districtsByCommunity;
	var _communitiesNamesById;

	// Community gender and age
	var _community_gender_age;

	// Chicago race and place of origin
	var _racesByCommunity;
	var _communityPlaceOfOrigin;

	
	// PUBLIC METHODS

	// Supply data methods
	this.getGeoJSON = function() {
		return _geoJSON;
	}

	this.getChicagoDistricts = function() {
		return Object.keys(_communitiesByDistrict);
	}

	this.getCommunitiesIdForDistrict = function(districtName) {
		return _communitiesByDistrict[districtName];
	}

	this.getCommunityNameForId = function(id) {
		return _communitiesNamesById[id];
	}

	this.districtOfCommunity = function(id) {
		return _districtsByCommunity[id];
	}

	this.getCommunitiesNumber = function() {
		var districts = self.getChicagoDistricts();
		var number = 0;
		districts.forEach(function(district) {
			number += self.getCommunitiesIdForDistrict(district).length;
		});
		return number;
	}

	this.getAllCommunitiesIds = function() {
		var districts = self.getChicagoDistricts();
		var ids = [];
		districts.forEach(function(district) {
			var communitiesIds = self.getCommunitiesIdForDistrict(district);
			ids = ids.concat(communitiesIds);
		});
		return ids;
	}

	this.getNumberOfPeopleInAgeRange = function(id, min, max) {
		var distribution = self.getAgeDistributionForCommunity(id,103);
		var number = 0;

		if (max != undefined) {
			distribution.forEach(function(ageInterval) {
				if (ageInterval.range.min() >= min && 
					ageInterval.range.max() != undefined && 
					ageInterval.range.max() <= max) {
					number += ageInterval.number;
				}
			});	
		} else {
			distribution.forEach(function(ageInterval) {
				if (ageInterval.range.min() >= min) {
					number += ageInterval.number;
				}
			});
		}
		return number;
	}

	this.getAverageAgeForCommunity = function(id) {
		var sum = 0;
		var distribution = self.getAgeDistributionForCommunity(id,103);

		distribution.forEach(function(ageInterval) {
				sum += (ageInterval.range.min() * ageInterval.number);
			});	
		return (sum / self.getNumberOfPeopleInAgeRange(id, 0, undefined));
	}

	this.getAgeDistributionForCommunity = function(id, numberOfRanges) {
		var maleNumberByAge = _community_gender_age[id].male;
		var femaleNumberByAge = _community_gender_age[id].female;
		var total = [];
		var resultDistribution = [];

		// Sum up male and female
		maleNumberByAge.forEach(function(el, i) {
			var totalNumber = el.number + femaleNumberByAge[i].number;
			total[i] = {
				range : el.range,
				number : totalNumber
			};
		});

		// Check coherency of the request
		if (numberOfRanges <= 0 || numberOfRanges > total.length) {
			return null;
		}

		// Merge ranges
		var mergeNumber = Math.floor(total.length / numberOfRanges);

		for (var i = 0; i < total.length; i+=mergeNumber) {
			var newRange = total[i].range;
			var newNumber = +total[i].number;
			var j = 1;
			while (j < mergeNumber && total[i+j] != undefined) {
				newRange = newRange.merge(total[i+j].range);
				newNumber += +total[i+j].number;
				j++;
			}

			resultDistribution.push({
				range : newRange,
				number : newNumber
			});
		}

		return resultDistribution;
	}

	// Return a dictionary of races (race is the key)
	this.getRacesDistributionForCommunity = function(id) {
		return _racesByCommunity[id];
	}

	this.getGenderDistributionForCommunity = function(id) {
		var resultDistribution = [];

		var totalMale = 0;
		_community_gender_age[id].male.forEach(function(el) {
			totalMale += el.number;
		});

		var totalFemale = 0;
		_community_gender_age[id].female.forEach(function(el) {
			totalFemale += el.number;
		});

		resultDistribution = {
			male : totalMale,
			female : totalFemale
		};

		return resultDistribution;
	}

	this.getCountriesDistributionForCommunity = function(id) {
		var resultDistribution;

		resultDistribution = _communityPlaceOfOrigin[id];

		return resultDistribution;
	}

	// Load data methods
	this.loadData = function() {
		enqueueOperations(
			function() {
				return loadMapData("data/chicago.json", operationDidFinish);
			},
			function() {
				return loadChicagoDistricts("data/chicago_districts.csv", operationDidFinish);
			},
			function() {
				return loadChicagoCommunitiesNames("data/chicago_communities.csv", operationDidFinish);
			},
			function() {
				return loadAgeAndGender("data/chicago_age_gender.csv", operationDidFinish);
			},
			function() {
				return loadPlaceOfOrigin("data/chicago_place_of_origin.csv", operationDidFinish);
			},
			function() {
				return loadRaces("data/chicago_races.csv", operationDidFinish);
			}
		);
	}

	// Other methods
	this.setDelegate = function(newDelegate) {
		_delegate = newDelegate; 
	}


	// PRIVATE METHODS

	var operationDidFinish = function() {
		_operationsQueue.pop();
    	if (_operationsQueue.length == 0) {
    		console.log("all data loaded!");
    		_delegate.dataLoaded();
   		}
	}	

	var loadMapData = function(path, callBack) {
		d3.json(path, function(json) {
    		_geoJSON = json;
    		callBack.apply();
		});
	}

	var loadChicagoDistricts = function(path, callBack) {
		d3.csv(path, function(csv) {
			_communitiesByDistrict = {};
			_districtsByCommunity = {};

			for (var i = 0; i < csv.length; i++) {
				if (_communitiesByDistrict[csv[i].side_name] == undefined) {
					_communitiesByDistrict[csv[i].side_name] = [];
				}
				_communitiesByDistrict[csv[i].side_name].push(parseInt(csv[i].community_area_number));
				_districtsByCommunity[parseInt(csv[i].community_area_number)] = csv[i].side_name;
			}

			callBack.apply();
		});
	}

	var loadChicagoCommunitiesNames = function(path, callBack) {
		d3.csv(path, function(csv) {
			_communitiesNamesById = {};

			for (var i = 0; i < csv.length; i++) {
				_communitiesNamesById[parseInt(csv[i].area)] = csv[i].name;
			}

			callBack.apply();
		});
	}

	var loadAgeAndGender = function(path, callBack) {
		d3.csv(path, function(csv) {
			_community_gender_age = {};
			
			csv.forEach(function(row) {
				if (_community_gender_age[row.community] == undefined) {
					_community_gender_age[row.community] = {};
				}
				//
				var unitRangeRegexp = "^[0123456789]+$";
				var rangeRegRegexp = "([0-9]+)\-([0-9]+)";
				var rangeOverRegexp = "^([0-9]+)\\+$";
				//var ageDict = {};
				var numberByAge = [];
				Object.keys(row).forEach(function(key) {
					var range;
					var match = key.match(unitRangeRegexp);
					if (match != null) {
						var unit = parseInt(key);
						range = new Range(unit, unit+1);
					} else {
						match = key.match(rangeRegRegexp);
						if (match != null) {
							var min = parseInt(match[1]);
							var max = parseInt(match[2]); 
							range = new Range(min, max);
						} else {
							match = key.match(rangeOverRegexp);
							if (match != null) {
								var lowBound = parseInt(match[1]);
								range = new Range(lowBound, undefined);
							}
						}
					}  
					if (range != undefined) {
						//ageDict[range] = parseInt(row[key]);
						numberByAge.push({
							range: range,
							number : parseInt(row[key])
						});
					}
				});
				//_community_gender_age[row.community][row.gender] = ageDict;
				_community_gender_age[row.community][row.gender] = numberByAge;
			});


			callBack.apply();
		});
	}















	var loadRaces = function(path, callBack) {
		d3.csv(path, function(csv) {
			_racesByCommunity = {};

			csv.forEach(function(row) {
				var communityId = row["community"];
				var races = _.without(d3.keys(row), "community");

				if (_racesByCommunity[communityId] == undefined) {
					_racesByCommunity[communityId] = {};
				}

				races.forEach(function(el) {
					var numberOfPeople = row[el];
					_racesByCommunity[communityId][el] = numberOfPeople;
				});
			});

			callBack.apply();
		});
	}

	var loadPlaceOfOrigin = function(path, callBack) {
		d3.csv(path, function(csv) {
			_communityPlaceOfOrigin = {};

			/*
			_communityPlaceOfOrigin = {
				id: [{
					country: ..,
					number: ..
				}]
			}
			*/
			csv.forEach(function(row) {
				var countries = _.without(d3.keys(row), "community");

				var numberPerCountry = [];
				countries.forEach(function(el) {
					numberPerCountry.push({
						country: el,
						number: row[el]
					});
				});
				_communityPlaceOfOrigin[row.community] = numberPerCountry;
			});

			callBack.apply();
		});
	}
	
	var enqueueOperations = function() {
		for (var i = 0; i < arguments.length; i++) {
    		_operationsQueue.push(true);
  		}

  		for (var i = 0; i < arguments.length; i++) {
    		arguments[i].apply();
  		}
	}

	var init = function() {
		_operationsQueue = [];
	} ();
};



function Range(min, max) {
	var _min = min;
	var _max = max;

	this.min = function() {
		return _min;
	}

	this.max = function() {
		return _max;
	} 

	this.equals = function(other) {
		return _min == other.min() && _max == other.max();
	}

	this.toString = function() {
		var representation;

		representation = (_max == undefined) ? _min + "+" : _min + "-" + _max;
		return representation;
	}

	this.merge = function(other) {
		var nmin = Math.min(_min, other.min());
		var nmax = (_max == undefined || other.max() == undefined) ? undefined : Math.max(_max, other.max());
		return new Range(nmin, nmax);
	}
}










