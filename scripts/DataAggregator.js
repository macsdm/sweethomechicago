/*
	@class: DataAggregator
	A class that provide utilites to aggregate chicago community area data.


	@author: Massimo De Marchi
	@copyright 2014
*/
function DataAggregator (database) {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _database = database;

	// PUBLIC METHODS
	//@selection --> array of community ids
	this.getNameForSelection = function(selection) {
		var componentsNames = self.getComponentsNameForSelection(selection);
		var name = "mixed group";
		if (componentsNames.length == 1) {
			name = componentsNames[0];
		}
		return name;
	}

	this.getComponentsNameForSelection = function(selection) {
		var componentsNames = [];
		var tmpSelection = selection.concat([]);
		if (tmpSelection instanceof Array) {
			// Community
			if (tmpSelection.length == 1) {
				componentsNames.push(_database.getCommunityNameForId(tmpSelection[0]));
			}
			// Chicago 
			else if (tmpSelection.length == _database.getCommunitiesNumber()) {
				componentsNames.push("Chicago");
			}
			// District or mixed
			else if (tmpSelection.length > 1) {
				var districts = _database.getChicagoDistricts();

				districts.forEach(function(district) {
					var communities = _database.getCommunitiesIdForDistrict(district);
					if (tmpSelection.containsAllElementsOfArray(communities)) {
						componentsNames.push(district);
						tmpSelection = _.difference(tmpSelection, communities);
					}
				});

				tmpSelection.forEach(function(id) {
					componentsNames.push(_database.getCommunityNameForId(id));
				});
			} 
		}
		return componentsNames;
	}

	// PRIVATE METHODS	
	var init = function() {
		
	} ();
};









