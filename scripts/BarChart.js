/*
@class: BarChart
A parametric class that handle the displaying of barcharts.

@Data format
{
	label: ..
	value: ..
}

@Protocols

@author: Massimo De Marchi
@copyright 2014
*/
function BarChart(svg) {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _svg = svg;
	var _boxWidth = 600;
	var _boxHeight = 300;

	var _data;
	var _title;
	var _xAxisLabel;
	var _yAxisLabel;
	var _colors;
	var _removeZeroValues;
	var _mainComponentsNumber;
	var _maxValue;

	// PUBLIC METHODS
	this.draw = function() {
		var margin = {top: 80, right: 150, bottom: 0, left: 50};
		var width = _boxWidth - margin.left - margin.right;
		var height = _boxHeight - margin.top - margin.bottom;

		/*
		var x = d3.scale.ordinal()
		.rangeRoundBands([0, width], .1);
		*/

		if (_removeZeroValues) {
			dataRemoveZeros();
		}

		if (_mainComponentsNumber != undefined) {
			reduceData();
		}

		y = d3.scale.linear()
				.range([0, width]);
				y.domain([0, d3.max(_data, function(d) { 
					if (_maxValue != undefined) {
						return parseInt(_maxValue);
					}
					return parseInt(d.value); 
				})]);

		var yAxis = d3.svg.axis()
						.scale(y)
						.orient("top")
						.ticks(6)
						.tickFormat(formatNumber)
						.outerTickSize(1);
		

		var gBars = _svg.append("g")
							.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		// CHART
		var barHeight = height / _data.length;
		var bars = gBars
						.selectAll(".bar")
						.data(_data)
						.enter()
							.append("g")
								.classed("bar", true)
								.attr("transform", function(d,i) {
									return "translate(0," + (i*barHeight) + ")";
								});

		// Y axis
		var axisHeight = 10;
		_svg
			.append("g")
				.attr("transform", function() {
					return "translate(" + margin.left + ","+ (margin.top-axisHeight) +" )";	
				})
				.attr("class", "y axis")
				.call(yAxis)
					.append("text")
				    	//.attr("transform", "translate(0, "+ (-30) +" )")
					    .attr("y", 6)
					    .attr("dy", ".71em")
					    .style("text-anchor", "middle")
					    .text(function() {
					    	return _yAxisLabel;
					    });

		
		bars
			.append("rect")
				.attr("height", function(d) { 
					return barHeight-1; 
				})
				.attr("width", function(d) {
					return y(d.value);
				})
				.style("fill", function(d) { 
					return _colors[0]; 
				});
		
		bars
			.append("text")
				.attr("transform", function(d, i) {
					return "translate(" + y(d.value) + "," + (barHeight/2) +  ")";
				})
				.attr("dy", ".35em")
				.attr("dx", "3")
				.style("text-anchor", "start")
				.text(function(d) { 
					return d.label; 
				});
		
		// Title
		var gTitle = _svg.append("g")
						.attr("transform", "translate(" + _boxWidth/2 + ",0)");
		gTitle
			.append("text")
        		.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return _title; 
				});

	}

	this.setData = function(newData) {
		_data = newData;
	}

	this.setTitle = function(newTitle) {
		_title = newTitle;
	}

	this.setXAxisLabel = function(label) {
		_xAxisLabel = label;
	}

	this.setYAxisLabel = function(label) {
		_yAxisLabel = label;
	}

	this.setColors = function(newColors) {
		_colors = newColors;
	}

	this.removeZeroValues = function(flag) {
		_removeZeroValues = flag;
	}

	this.setMainComponentsNumber = function(number) {
		_mainComponentsNumber = number;
	}

	this.setMaxValue = function(newMaxValue) {
		_maxValue = newMaxValue;
	}

	// PRIVATE METHODS
	var type = function(d) {
		d.value = +d.value; // coerce to number
		return d;
	}

	var dataRemoveZeros = function() {
		_data = _data.filter(function(el) {
			return el.value > 0;
		});
	}

	var reduceData = function() {
		_data = _data.sort(function(a, b) {
		  	return b.value - a.value;
		});
		var mainComponents = _.first(_data, _mainComponentsNumber);
		var other = _.rest(_data, _mainComponentsNumber);
		var otherValueSum = d3.sum(other, function(el) {
			return el.value;
		});
		_data = mainComponents;
		_data.push({
			label : "others",
			value : otherValueSum
		});
	}

	var init = function() {
		_svg
			.attr("viewBox", "0 0 " + _boxWidth + " " + _boxHeight)
			.attr("preserveAspectRatio","xMidYMid meet");
		_xAxisLabel = "";
		_yAxisLabel = "";
		_removeZeroValues = false;
	} ();
}










