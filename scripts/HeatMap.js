/*
@class: HeatMap
A parametric class that handle the displaying of a heat map.

@Protocols

@author: Massimo De Marchi
@copyright 2014
*/
function HeatMap (svg) {
	// PRIVATE
	var self = this;

	// Data
	var _geoJSON;
	var _data;

	var _title;
	var _numberOfQuantiles = 5;

	// Settings
	var _svg = svg;
	var _boxWidth = 600;
	var _boxHeight = 600;

	// PUBLIC METHODS
	this.draw = function() {
		var margin = {top: 30, right: 10, bottom: 50, left: 10};
		var padding = {top: 40, right: 10, bottom: 40, left: 10};
		var width = _boxWidth - margin.left - margin.right - padding.left - padding.right;
		var height = _boxHeight - margin.top - margin.bottom - padding.top - padding.bottom;

		var rateById = d3.map(_data, function(d) {
			return d.id;
		});

		var domain = calculateDomain();
		// Create the quantize scale
		var quantize = d3.scale.quantize()
    					.domain(domain)
    					.range(d3.range(_numberOfQuantiles).map(function(i) { return "q" + i + "-9"; }));

    	// Emmpty svg
    	_svg.html("");

    	// Calculate center coordinate for the map
    	var b = d3.geo.bounds(_geoJSON);
    	var geoWidth = longitudeDistance(b);
    	var geoHeight = latitudeDistance(b);
    	var geoCenter = [meanLongitude(b), meanLatitude(b)];

    	// Calculate offset to center the map
    	var offset = [width/2, height/2];

    	// Create map projection
    	var adjFactor = 4700; // is a correction scaling factor yet to be understood;
		var s = Math.min(width / geoWidth, height / geoHeight);
		s *= adjFactor;	// Adjustment factor
		var projection = d3.geo.mercator().scale(s).center(geoCenter).translate(offset);
		var path = d3.geo.path().projection(projection);

		var gMap = _svg
					.append("g")
						.classed("heatmap-box", true)
						.attr("transform", function() {
							return "translate(" + (margin.left + padding.left) + "," + (margin.top + padding.top) + ")";
						});

		var paths = gMap.selectAll("path").data(_geoJSON.features);

		paths
			.enter()
			.append("path")
			.attr("class", function(d) { 
				return quantize(rateById.get(d.id)); 
			})
			.classed("heatmap-feature", true)
      		.attr("d", path)
      		.attr("stroke", "#fff")
			.attr("stroke-width", "1.0")
      		.on('click', function(d) {
      			var nameLabel = gMap.selectAll(".comm-name").data([d])
      									.attr("transform", function(d) { 
						    				return "translate(" + path.centroid(d) + ")"; 
						    			})
						    			.text(function(d) { return d.properties.name; });

      			nameLabel
      				.enter()
      				.append("text")
		    			.attr("class", "comm-name")
		    			.attr("transform", function(d) { 
		    				console.log(d);
		    				return "translate(" + path.centroid(d) + ")"; 
		    			})
		    			.attr("dy", ".35em")
		    			.style("font-size", "12px")
		    			.style("font-weight", "600")
		    			.style("text-anchor", "middle")
		    			.text(function(d) { return d.properties.name; });

		    	nameLabel.exit().remove();
      		});

      	// Title
      	var gTitle = _svg.append("g")
						.attr("transform", "translate(" + _boxWidth/2 + "," + margin.top + ")");
		gTitle
			.append("text")
				.classed("heatmap-title", true)
        		.attr("dy", ".75em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return _title; 
				});


		var labelsInfo = [];
		var min = domain[0];
		var max = domain[1];
		var qScale = (max - min) / _numberOfQuantiles;
		for(var i = 0; i < _numberOfQuantiles; i++) {
			labelsInfo[i] = min + qScale * (i+1);
		}

		// LABELS
		var gLabels = _svg.append("g")
						.attr("transform", "translate(" + margin.left + "," + (margin.top + height + (padding.bottom)) + ")");

		var labels = gLabels.selectAll(".heatmap-quantiles-label").data(labelsInfo)
						.enter()
							.append("g")
							.classed(".heatmap-quantiles-label", true);
		labels
			.append("circle")
				.attr("cx", function (d, i) { 
					return (i+1) * ((width + padding.left + padding.right) / (labelsInfo.length+1)); 
				})
				.attr("cy", "20")
				.attr("r", "10")
                .attr("class", function(d,i) {
                	return ("q" + i + "-9");
                });
        labels
        	.append("text")
        		.attr("transform", function(d, i) { 
					return "translate(" + ((i+1) * ((width + padding.left + padding.right) / (labelsInfo.length+1))) + ", 50)"; 
				})
				.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return formatNumber(d); 
				});
	}

	this.setFeatures = function(features) {
		_geoJSON = features;
	}

	this.setData = function(data) {
		_data = data;
	}

	this.setTitle = function(title) {
		_title = title
	}


	// PRIVATE METHODS
	var calculateDomain = function() {
		var values = d3.values(_data);
		var min = d3.min(values);
		var max = d3.max(values);
		//console.log("min# ", min);
		//console.log("max# ", max);
		return [min, max];
	}

	// Km as result unit measure
	var longitudeDistance = function(bounds) {
		var lonPoints = [[bounds[0][0],bounds[0][1]],[bounds[1][0],bounds[0][1]]];
		return geoDistance(lonPoints);
	}

	// Km as result unit measure
	var latitudeDistance = function(bounds) {
		var latPoints = [[bounds[0][0],bounds[0][1]],[bounds[0][0],bounds[1][1]]];
		return geoDistance(latPoints);
	}

	var meanLongitude = function(bounds) {
		return (bounds[1][0]+bounds[0][0])/2;
	}

	var meanLatitude = function(bounds) {
		return (bounds[1][1]+bounds[0][1])/2;
	}

	var geoDistance = function(points) {
		var R = 6371; // km
		var lat1 = points[0][1];
		var lat2 = points[1][1];
		var lon1 = points[0][0];
		var lon2 = points[1][0];
		var phi1 = lat1.toRadians();
		var phi2 = lat2.toRadians();
		var delta_phi = (lat2 - lat1).toRadians();
		var delta_lambda = (lon2 - lon1).toRadians();

		var a = Math.sin(delta_phi/2) * Math.sin(delta_phi/2) +
		Math.cos(phi1) * Math.cos(phi2) *
		Math.sin(delta_lambda/2) * Math.sin(delta_lambda/2);
		var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

		var d = R * c;
		return d;
	}

	var init = function() {
		_svg
			.attr("viewBox", "0 0 " + _boxWidth + " " + _boxHeight)
			.attr("preserveAspectRatio","xMidYMid meet");
	} ();
}