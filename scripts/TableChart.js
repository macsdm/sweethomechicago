/*
@class: TableChart
A parametric class that handle the displaying of piecharts.

@Protocols

@author: Massimo De Marchi
@copyright 2014
*/
function TableChart (svg) {
	// PRIVATE ATTRIBUTES
	var self = this;

	// Data
	var _data;
	var _colors;
	var _title;

	// Display settings
	var _svg = svg;

	var _boxWidth = 600;
	var _boxHeight = 450;

	var _removeZeroValues;
	var _mainComponentsNumber;

	// PUBLIC METHODS
	this.draw = function() {
		var margin = {top: 30, right: 50, bottom: 80, left: 50};

		var width = _boxWidth - margin.right - margin.left;
		var height = _boxHeight - margin.top - margin.bottom;

		var color = d3.scale.ordinal()
			.range(_colors);

		if (_removeZeroValues) {
			dataRemoveZeros();
		}

		if (_mainComponentsNumber != undefined) {
			reduceData();
		}

		addTotal();
	

		var table = _svg.append("g")
						.attr("transform", "translate(" + margin.left + "," + margin.top + ")");	

		var rowHeight = height / _data.length;
		var columnWidth = width / 2;
		var rows = table
						.selectAll(".table-rows")
						.data(_data)
						.enter()
							.append("g")
								.classed(".table-rows", true)
								.attr("transform", function(d,i) {
									return "translate(0," + (i*rowHeight) + ")";
								});

		// Table header
		rows
			.append("rect")
				.classed("row-header", true)
				.attr("height", function(d) { 
					return rowHeight-1; 
				})
				.attr("width", columnWidth)
				.style("fill", function(d) { 
					if (_colors.length < _data.length) {
						return _colors[0];
					}
					return color(d.label); 
				});

		rows
			.append("text")
				.attr("transform", "translate(" + columnWidth/2 + "," + rowHeight/2 + ")")
				.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return d.label; 
				});

		// Table column
		rows
			.append("rect")
				.attr("transform", "translate(" + columnWidth + ",0)")
				.classed("column-header", true)
				.attr("height", function(d) { 
					return rowHeight -3; 
				})
				.attr("width", columnWidth)
				.style("fill", "#F8F8F8");

		var rightPadding = 20;
		rows
			.append("text")
				.attr("transform", "translate(" + (columnWidth*2 -rightPadding) + "," + rowHeight/2 + ")")
				.attr("dy", ".35em")
				.style("text-anchor", "end")
				.text(function(d) { 
					return numericFormat(parseInt(d.value)); 
				});



		var gTitle = _svg.append("g")
						.attr("transform", "translate(" + _boxWidth/2 + ",0)");
		gTitle
			.append("text")
        		.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return _title; 
				});
	}

	this.setData = function(newData) {
		_data = newData;
	}

	this.setColors = function(newColors) {
		_colors = newColors;
	}

	this.setTitle = function(newTitle) {
		_title = newTitle;
	}

	this.removeZeroValues = function(flag) {
		_removeZeroValues = flag;
	}

	this.setMainComponentsNumber = function(number) {
		_mainComponentsNumber = number;
	}

	// PRIVATE METHODS
	var addTotal = function() {
		var total = d3.sum(_data, function(el) {
			return parseInt(el.value);
		});

		_data.push({
			label : "total",
			value : total
		});

		// add color
		_colors.push("#ddd");
	}

	var dataRemoveZeros = function() {
		_data = _data.filter(function(el) {
			return parseInt(el.value) > 0;
		});
	}

	var reduceData = function() {
		_data = _data.sort(function(a, b) {
		  	return parseInt(b.value) - parseInt(a.value);
		});
		var mainComponents = _.first(_data, _mainComponentsNumber);
		var other = _.rest(_data, _mainComponentsNumber);
		var otherValueSum = d3.sum(other, function(el) {
			return parseInt(el.value);
		});
		_data = mainComponents;
		_data.push({
			label : "others",
			value : otherValueSum
		});
	}

	var init = function() {
		_svg
			.attr("viewBox", "0 0 " + _boxWidth + " " + _boxHeight)
			.attr("preserveAspectRatio","xMidYMid meet");
		_removeZeroValues = false;
	} ();
}









