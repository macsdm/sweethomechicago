// Database
var dataManager;
var dataAggregator;

// Notifications
var notificationCenter;

// Controllers
var selectorManager;
var dataSettingsManager;
var dataVisualizationManager;

// UI
var chicagoMap;
var areaSelectionTool;
var dataSettingsTool;

var main = function() {
	var self = this;

	window.onload = function() {
		dataManager = new DataManager();
		dataManager.setDelegate(self);
		dataManager.loadData();
	}

	this.dataLoaded = function() {
		init();
	}

	var init = function() {
		var svg = d3.select("#map-box .map-wrapper")
		.append("svg");

		// Data aggregator
		dataAggregator = new DataAggregator(dataManager);

		// Setup notification center to dispatch notifications across componentss
		notificationCenter = new NotificationCenter();

		// Manager of user areas selections 
		selectorManager = new SelectorManager();

		// Map
		chicagoMap = new Map(svg, dataManager.getGeoJSON());
		chicagoMap.setDelegate(selectorManager);
		chicagoMap.updateUI();	// This one should be avoided --> unresolved svg wrong width problem (chrome bug?)

		// Area selection toolbox
		var toolContainer = d3.select("#area-selection-tool-container");
		areaSelectionTool = new AreaSelectionTool(toolContainer, selectorManager);
		areaSelectionTool.setSelectorDelegate(selectorManager);
		areaSelectionTool.updateUI();

		// Data settings
		dataFilterManager = new DataFilterManager();
		dataSettingsTool = new DataFilterTool(dataFilterManager);

		// Data visualization
		dataVisualizationManager = new DataVisualizationManager(dataFilterManager, selectorManager);
	}
};

main();










