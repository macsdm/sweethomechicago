/*
	@class: DataFilterTool
	A class that handles the data settings (which data to show and which type of chart).

	@Protocols

	@author: Massimo De Marchi
	@copyright 2014
*/
function DataFilterTool(dataFilterManager) {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _dataFilterManager = dataFilterManager;

	// PUBLIC METHODS
	
	
	// PRIVATE METHODS
	var setup = function() {
		setupToolbar();
		setupDataTypeMenu();
	}	

	var setupToolbar = function() {
		// Barchart button
		d3.select("#barchart-button")
			.classed("bar-button-selected", true)
			.on('click', function() {
				_dataFilterManager.setVisualizationType(VisualizationType.BARCHART);
				d3.select("#data-visualization-box .bar-button-selected")
					.classed("bar-button-selected", false);
				d3.select(this)
					.classed("bar-button-selected", true);
				dataVisualizationManager.setComparisonType(ComparisonType.COMPARE);

				enableUI();
			});
		// piechart button
		d3.select("#pie-button")
			.on('click', function() {
				_dataFilterManager.setVisualizationType(VisualizationType.PIECHART);
				d3.select("#data-visualization-box .bar-button-selected")
					.classed("bar-button-selected", false);
				d3.select(this)
					.classed("bar-button-selected", true);
				dataVisualizationManager.setComparisonType(ComparisonType.COMPARE);

				enableUI();
			});
		// raw data button
		d3.select("#rawdata-button")
			.on('click', function() {
				_dataFilterManager.setVisualizationType(VisualizationType.RAWTABLE);
				d3.select("#data-visualization-box .bar-button-selected")
					.classed("bar-button-selected", false);
				d3.select(this)
					.classed("bar-button-selected", true);
				dataVisualizationManager.setComparisonType(ComparisonType.COMPARE);

				enableUI();
			});
	}

	var enableUI = function() {
		d3.select("#area-button-wrapper").style("display", "block");
		d3.select("#select-current-highlight").style("display", "block");
		d3.select("#visualize").style("display", "block");
		d3.select(".map-wrapper").style("display", "block");
	}

	var setupDataTypeMenu = function() {
		// Age button
		d3.select("#age-button")
			.classed("data-button-selected", true)
			.on('click', function() {
				_dataFilterManager.setDataType(DataType.AGE);
				d3.select("#data-selection-box .data-button-selected")
					.classed("data-button-selected", false);
				d3.select(this)
					.classed("data-button-selected", true);
			});
		// Gender button
		d3.select("#gender-button")
			.on('click', function() {
				_dataFilterManager.setDataType(DataType.GENDER);
				d3.select("#data-selection-box .data-button-selected")
					.classed("data-button-selected", false);
				d3.select(this)
					.classed("data-button-selected", true);
			});
		// Race button
		d3.select("#race-button")
			.on('click', function() {
				_dataFilterManager.setDataType(DataType.RACE);
				d3.select("#data-selection-box .data-button-selected")
					.classed("data-button-selected", false);
				d3.select(this)
					.classed("data-button-selected", true);
			});
		// Place of origin button
		d3.select("#place-of-origin-button")
			.on('click', function() {
				_dataFilterManager.setDataType(DataType.PLACE_OF_ORIGIN);
				d3.select("#data-selection-box .data-button-selected")
					.classed("data-button-selected", false);
				d3.select(this)
					.classed("data-button-selected", true);
			});
	}

	var init = function() {
		setup();
	} ();
}
















