/*
	@class: HeatMapTool
	A class that handles the visualization heatmap.

	@Protocols

	@author: Massimo De Marchi
	@copyright 2014
*/
function HeatMapTool () {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _optionsMenuTool;
	var _heatmap;
	var _dataType;

	// PUBLIC METHODS
	this.draw = function(dataType) {
		var container = d3.select("#visualization-compare-wrapper");
		
		var heatMapSvg = container
					.append("div")
						.classed("heatmap-container", true)
						.append("svg");

		var optionMenuSvg = d3.select("#visualization-compare-wrapper")
								.append("div")
									.classed("option-menu-tool-container", true)
									.append("svg");
		_optionsMenuTool = new OptionsMenuTool(optionMenuSvg, self);

		// Configure heatmap initial settings
		_heatmap = new HeatMap(heatMapSvg);
		_heatmap.setFeatures(dataManager.getGeoJSON());

		_dataType = dataType;
		update(0);
	}

	this.optionSelected = function(index) {
		update(index);
	}

	// PRIVATE METHODS
	var update = function(index) {
		if (_dataType == DataType.AGE) {
			_heatmap.setTitle("AGE DATA PER COMMUNITY");

			_optionsMenuTool.setButtonLabels(["Average age", "0-24", "25-49", "50-74", "75-99", "100+"]);
			_optionsMenuTool.setColors(["#2ca25f"]);

			_optionsMenuTool.draw();
			_heatmap.setData(dataForTypeAndIndex(_dataType, index));
			_heatmap.draw();
		}
	}

	var dataForTypeAndIndex = function(type, index) {
		var data = [];
		var communitiesIds = dataManager.getAllCommunitiesIds();

		if (_dataType == DataType.AGE) {
			if (index == 0) {
				communitiesIds.forEach(function(id) {
					data[id] = Math.floor(dataManager.getAverageAgeForCommunity(id));
				});	
			} else if(index == 1) {
				communitiesIds.forEach(function(id) {
					data[id] = dataManager.getNumberOfPeopleInAgeRange(id, 0, 24);
				});	
			} else if(index == 2) {
				communitiesIds.forEach(function(id) {
					data[id] = dataManager.getNumberOfPeopleInAgeRange(id, 25, 49);
				});	
			} else if(index == 3) {
				communitiesIds.forEach(function(id) {
					data[id] = dataManager.getNumberOfPeopleInAgeRange(id, 50, 74);
				});	
			} else if(index == 4) {
				communitiesIds.forEach(function(id) {
					data[id] = dataManager.getNumberOfPeopleInAgeRange(id, 75, 99);
				});	
			} else if(index == 5) {
				communitiesIds.forEach(function(id) {
					data[id] = dataManager.getNumberOfPeopleInAgeRange(id, 100, undefined);
				});	
			}
			
		}

		return data;
	}

	var init = function() {
		
	} ();
}









