/*
	@class: DataVisualizationManager
	A class that handles how to compare groups of selection.

	Implements NotificationCenter Observer protocol
	@Protocols

	@author: Massimo De Marchi
	@copyright 2014
*/
function DataVisualizationManager (dataFilterManager, selectionsManager) {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _comparisonType = ComparisonType.COMPARE;
	var _dataFilterManager = dataFilterManager;
	var _selectionsManager = selectionsManager;

	// Data visualization tools
	var _compareTool;
	var _heatMapTool;

	// PUBLIC METHODS
	this.setComparisonType = function(comparisonType) {
		_comparisonType = comparisonType;
		updateVisualization();
	}

	this.getComparisonType = function() {
		return _comparisonType;
	}

	this.removeSelectionAtIndex = function(index) {
		_selectionsManager.removeVisualizationAtIndex(index);
	}
	
	//@Protocol Observer
	this.notify = function(notification) {
		updateVisualization();
	}

	// PRIVATE METHODS	
	var updateVisualization = function() {
		var dataType = _dataFilterManager.getDataType();

		if (_comparisonType == ComparisonType.COMPARE || _comparisonType == ComparisonType.SHOWDIFFERENCE) {
			d3.select("#visualization-compare-wrapper").html("");
			var chartType = _dataFilterManager.getVisualizationType();
			var selectionsGroups = _selectionsManager.getSelectedGroups();

			_compareTool.draw(selectionsGroups, dataType, chartType);
		} else if (_comparisonType == ComparisonType.HEATMAP) {
			d3.select("#visualization-compare-wrapper").html("");

			_heatMapTool.draw(dataType);
		}
	}


	var registerEvents = function() {
		d3.select("#heatmap-button")
			.on('click', function() {
				self.setComparisonType(ComparisonType.HEATMAP);
				d3.select("#data-visualization-box .bar-button-selected")
					.classed("bar-button-selected", false);
				d3.select(this)
					.classed("bar-button-selected", true);

				// Hide unused interface
				d3.select("#area-button-wrapper").style("display", "none");
				d3.select("#select-current-highlight").style("display", "none");
				d3.select("#visualize").style("display", "none");
				d3.select(".map-wrapper").style("display", "none");
			});

		d3.select("#differences-button")
			.on('click', function() {
				self.setComparisonType(ComparisonType.SHOWDIFFERENCE);
				d3.select("#data-visualization-box .bar-button-selected")
					.classed("bar-button-selected", false);
				d3.select(this)
					.classed("bar-button-selected", true);

				// Hide unused interface
				d3.select("#area-button-wrapper").style("display", "block");
				d3.select("#select-current-highlight").style("display", "block");
				d3.select("#visualize").style("display", "block");
				d3.select(".map-wrapper").style("display", "block");
			});
	}

	var init = function() {
		_comparisonType = ComparisonType.COMPARE;

		_compareTool = new CompareVisualizationTool(self);
		_heatMapTool = new HeatMapTool();

		// Register events
		registerEvents();

		// Subscribe to notifications
		notificationCenter.subscribe(self, Notifications.DATA_TYPE_CHANGED);
		notificationCenter.subscribe(self, Notifications.VISUALIZATION_TYPE_CHANGED);
		notificationCenter.subscribe(self, Notifications.VISUALIZATION_GROUPS_CHANGED);

		updateVisualization();
	} ();
};

var ComparisonType = {
	COMPARE : 0,
	SHOWDIFFERENCE : 1,
	HEATMAP : 2
};