/** Converts numeric degrees to radians */
if (typeof(Number.prototype.toRad) === "undefined") {
	Number.prototype.toRadians = function() {
		return this * Math.PI / 180;
	}
}

// attach the .equals method to Array's prototype to call it on any array
Array.prototype.equals = function (array) {
    // if the other array is a falsy value, return
    if (!array)
        return false;

    // compare lengths - can save a lot of time 
    if (this.length != array.length)
        return false;

    for (var i = 0, l=this.length; i < l; i++) {
        // Check if we have nested arrays
        if (this[i] instanceof Array && array[i] instanceof Array) {
            // recurse into the nested arrays
            if (!this[i].equals(array[i]))
                return false;       
        }           
        else if (this[i] != array[i]) { 
            // Warning - two different object instances will never be equal: {x:20} != {x:20}
            return false;   
        }           
    }       
    return true;
} 

Array.prototype.intersects = function(array) {
    // if the other array is a falsy value, return
    if (!array) {
        return false;
    }

    var i = 0;
    var j;
    while(i < this.length) {
        j = 0;
        while(j < array.length) {
            if (this[i] == array[j]) {
                return true;
            }
            j++;
        }
        i++;
    }
    return false;
}

Array.prototype.containsAllElementsOfArray = function(array) {
    // if the other array is a falsy value, return
    if (!array) {
        return false;
    }

    return _.intersection(this, array).equals(array);
}

var us_US = {
  "decimal": ".",
  "thousands": ",",
  "grouping": [3],
  "currency": ["$", ""],
  "dateTime": "%a %b %e %X %Y",
  "date": "%m/%d/%Y",
  "time": "%H:%M:%S",
  "periods": ["AM", "PM"],
  "days": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
  "shortDays": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
  "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
  "shortMonths": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
};

var USLocale = d3.locale(us_US);

var numericFormat = d3.format("123,456,789");


var formatNumber = function(n) {
    var s = n;
    var unit = "";
    if (n >= 1000 && n < 1000000) {
      s = numericFormat(n / 1000);
      unit = "k";
      var decimals = s.split(".")[1];
      var integer = s.split(".")[0];
      if (decimals != undefined) {
        s = integer + "." + decimals[0];
      }
    } else if (n >= 1000000) {
      s = numericFormat(n / 1000000);
      unit = "M";
      var decimals = s.split(".")[1];
      var integer = s.split(".")[0];
      if (decimals != undefined) {
        s = integer + "." + decimals[0];
      }
    }

    return (s + unit);
  }
















