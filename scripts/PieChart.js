/*
@class: PieChart
A parametric class that handle the displaying of piecharts.

@Protocols

@author: Massimo De Marchi
@copyright 2014
*/
function PieChart (svg) {
	// PRIVATE ATTRIBUTES
	var self = this;

	// Data
	var _data;
	var _colors;
	var _title;

	// Display settings
	var _svg = svg;

	var _boxWidth = 800;
	var _boxHeight = 300;
	var _removeZeroValues;
	var _mainComponentsNumber;

	// PUBLIC METHODS
	this.draw = function() {
		var margin = {top: 30, right: 10, bottom: 80, left: 10};

		var width = _boxWidth - margin.right - margin.left;
		var height = _boxHeight - margin.top - margin.bottom;

		var radius = Math.min(width, height) / 2;

		// Clean data
		if (_removeZeroValues) {
			dataRemoveZeros();
		}

		if (_mainComponentsNumber != undefined) {
			reduceData();
		}

		var color = d3.scale.ordinal()
			.range(_colors);

		// Function that return arc drawing path
		var arc = d3.svg.arc()
					.outerRadius(radius)
					.innerRadius(0);

		// Function that calculate arc size based on data
		var pie = d3.layout.pie()
					.sort(null)
					.value(function(d) { 
						return d.value; 
					});

		// Calculate total sum to allow percentage computation
		var tot = 0;
		_data.forEach(function(el) {
			tot += parseInt(el.value);
		});

		var chart = _svg.append("g")
						.attr("transform", "translate(" + _boxWidth / 2 + "," + _boxHeight / 2 + ")");


		var arcs = chart.selectAll(".arc").data(pie(_data))
					.enter()
						.append("g")
						.attr("class", "arc");

		// Draw arcs
		arcs.append("path")
			.attr("d", arc)
			.style("fill", function(d) { 
				/* cos'è "d"?
					{
						data : {label : .., value : ..},
						value : ..,
						startAngle : ..
						endAngle : ..
					}
				*/
				return color(d.data.label); 
			});

		// Percentage
		arcs.append("text")
			.attr("transform", function(d) { 
				return "translate(" + arc.centroid(d) + ")"; 
			})
			.attr("dy", ".35em")
			.style("text-anchor", "middle")
			.text(function(d) { 
				var percent = Math.round((parseInt(d.data.value) / tot) * 100);
				return (percent > 5) ? (percent + "%") : ""; 
			});

		var gLabels = _svg.append("g")
						.attr("transform", "translate(" + margin.left + "," + (margin.top + (radius*2) + (margin.bottom/2)) + ")");

		var labels = gLabels.selectAll(".pie-label").data(pie(_data))
						.enter()
							.append("g")
							.classed("arc", true);
		labels
			.append("circle")
				.attr("cx", function (d, i) { 
					return (i+1) * (width / (_data.length+1)); 
				})
				.attr("cy", "20")
				.attr("r", "10")
                .style("fill", function(d) { 
                	return color(d.data.label); 
                });
        labels
        	.append("text")
        		.attr("transform", function(d, i) { 
					return "translate(" + ((i+1) * (width / (_data.length+1))) + ", 50)"; 
				})
				.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return d.data.label; 
				});

		var gTitle = _svg.append("g")
						.attr("transform", "translate(" + _boxWidth/2 + ",0)");
		gTitle
			.append("text")
        		.attr("dy", ".35em")
				.style("text-anchor", "middle")
				.text(function(d) { 
					return _title; 
				});
	}

	this.setData = function(newData) {
		_data = newData;
	}

	this.setColors = function(newColors) {
		_colors = newColors;
	}

	this.setTitle = function(newTitle) {
		_title = newTitle;
	}

	this.removeZeroValues = function(flag) {
		_removeZeroValues = flag;
	}

	this.setMainComponentsNumber = function(number) {
		_mainComponentsNumber = number;
	}

	// PRIVATE METHODS
	var dataRemoveZeros = function() {
		_data = _data.filter(function(el) {
			return el.value > 0;
		});
	}

	var reduceData = function() {
		_data = _data.sort(function(a, b) {
		  	return b.value - a.value;
		});
		var mainComponents = _.first(_data, _mainComponentsNumber);
		var other = _.rest(_data, _mainComponentsNumber);
		var otherValueSum = d3.sum(other, function(el) {
			return el.value;
		});
		_data = mainComponents;
		_data.push({
			label : "others",
			value : otherValueSum
		});
		console.log(_data);
	}

	var init = function() {
		_svg
			.attr("viewBox", "0 0 " + _boxWidth + " " + _boxHeight)
			.attr("preserveAspectRatio","xMidYMid meet");
		_removeZeroValues = false;
	} ();
}