/*
	@class: NotificationCenter
	A class that manages dispatch of notification to observers.

	@Protocols
	- notify(notification) --> Observers must implement this

	@author: Massimo De Marchi
	@copyright 2014
*/
function NotificationCenter () {
	// PRIVATE ATTRIBUTES
	var self = this;

	var _observers;

	// PUBLIC METHODS
	this.dispatch = function(notification) {
		if (_observers[notification] != undefined) {
			_observers[notification].forEach(function(observer) {
				observer.notify(notification);
			});
		}
	}

	this.subscribe = function(observer, notification) {
		if (_observers[notification] == undefined) {
			_observers[notification] = [];
		}
		_observers[notification].push(observer);
	}
	

	// PRIVATE METHODS	
	var init = function() {
		_observers = {};
	} ();
};

var Notifications = {
	DATA_TYPE_CHANGED : 0,
	VISUALIZATION_TYPE_CHANGED : 1,
	VISUALIZATION_GROUPS_CHANGED : 2
};