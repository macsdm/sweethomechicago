/*
	@class: DataFilterManager
	A class that manages the data settings for the visualization.

	@Protocols

	@author: Massimo De Marchi
	@copyright 2014
*/
function DataFilterManager () {
	// PRIVATE ATTRIBUTES
	var self = this;

	// Visualization manager --> notify when settings change

	var _currentDataType = DataType.AGE;
	var _currentVisualizationType = VisualizationType.BARCHART;

	// PUBLIC METHODS
	this.setDataType = function(dataType) {
		_currentDataType = dataType;
		notificationCenter.dispatch(Notifications.DATA_TYPE_CHANGED);
	}

	this.getDataType = function() {
		return _currentDataType;
	}

	// Type of visualization settings
	this.setVisualizationType = function(vizType) {
		_currentVisualizationType = vizType;
		notificationCenter.dispatch(Notifications.VISUALIZATION_TYPE_CHANGED);
	}

	this.getVisualizationType = function() {
		return _currentVisualizationType;
	}

	// PRIVATE METHODS	
	var init = function() {
		_currentDataType = DataType.AGE;
		_currentVisualizationType = VisualizationType.BARCHART;
	} ();
};

var DataType = {
	AGE : 0,
	GENDER : 1,
	RACE : 2,
	PLACE_OF_ORIGIN : 3
}

var VisualizationType = {
	BARCHART : 0,
	PIECHART : 1,
	RAWTABLE : 2
}